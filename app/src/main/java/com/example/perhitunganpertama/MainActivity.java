package com.example.perhitunganpertama;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText no1, no2;
    TextView hasil;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        no1 = (EditText) findViewById(R.id.inputAngka1);
        no2 = (EditText) findViewById(R.id.inputAngka2);
        hasil=(TextView) findViewById(R.id.textViewHasil);
        button=(Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int angka1 = Integer.parseInt(no1.getText().toString());
                int angka2 = Integer.parseInt(no2.getText().toString());
                int hsl = angka1+angka2;
                hasil.setText(""+hsl);
            }
        });
    }
}